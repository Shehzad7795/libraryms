package com.abc.lms;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/AddBookDetails")
public class AddBookDetails extends HttpServlet{
	public void service(HttpServletRequest request,HttpServletResponse response) throws IOException {
		String bookid = request.getParameter("bookid");
		String category = request.getParameter("category");
		String bookname = request.getParameter("bookname");
		String authorname = request.getParameter("author");
		String edition = request.getParameter("edition");
		
		Model m = new Model();
		
	   m.setBookid(bookid);
	   m.setCategory(category);
	   m.setBookname(bookname);
	   m.setAuthorname(authorname);
	   m.setEdition(edition);
		
		
		
		boolean addBookDetails = m.addBookDetails();
		if(addBookDetails==true) {
		
			response.sendRedirect("addBookDetailsSuccess.jsp");
			
		}
		else {
			response.sendRedirect("loginFail.jsp");
		}
	}

}
