package com.abc.lms;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Model {

	private String username;
	private String password;
	
	private String adminname;
	private String adminpwd;
	
	private String fullname;
	private String mobile;
	private String email;
	private String gender;
	private String dob;
	private String adhaarno;
	private String name;
	private String pwd;
	
	
	private String bookid;
	private String category;
	private String bookname;
	private String authorname;
	private String edition;
	
    String url="jdbc:mysql://localhost:3306/lms";
    String un="root";
    String pw="root123";
    
    
    Connection con;
    ResultSet res;
    PreparedStatement pstmt; 
    Statement stmt;
//	setters and getters
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAdminname() {
		return adminname;
	}
	public void setAdminname(String adminname) {
		this.adminname = adminname;
	}
	public String getAdminpwd() {
		return adminpwd;
	}
	public void setAdminpwd(String adminpwd) {
		this.adminpwd = adminpwd;
	}
	

	
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAdhaarno() {
		return adhaarno;
	}
	public void setAdhaarno(String adhaarno) {
		this.adhaarno = adhaarno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	
	
	

	
	public String getBookid() {
		return bookid;
	}
	public void setBookid(String bookid) {
		this.bookid = bookid;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public String getAuthorname() {
		return authorname;
	}
	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	
	
//	Loading Driver and Established connection 
	 {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			System.out.println("Driver not loaded successfully");
			e.printStackTrace();
		}
		System.out.println("driver has been registered");
		try {
			con = DriverManager.getConnection(url, un, pw);
		} catch (SQLException e) {
			System.out.println("Driver connection was not success");
			e.printStackTrace();
		}
		System.out.println("connection established successfully");
	}
	
	public boolean login() {
		String sql="select * from lms.login where username=? and password=?";
		try {
			 pstmt = con.prepareStatement(sql);
			 pstmt.setString(1, username);
			 pstmt.setString(2, password);
			 res = pstmt.executeQuery();
			 if(res.next()==true) {
				 String name = res.getString("username");
				 return true;
			 }
		} catch (Exception e) {
			System.out.println("some problem occured in login () method " + e);
		}
		return false;
	   
	}
	public boolean adminLogin() {
		String sql="select * from lms.alogin where username=? and password=?";
		try {
			 pstmt = con.prepareStatement(sql);
			 pstmt.setString(1, adminname);
			 pstmt.setString(2, adminpwd);
			 res = pstmt.executeQuery();
			 if(res.next()==true) {
				 String name = res.getString("username");
				 return true;
			 }
		} catch (Exception e) {
			System.out.println("some problem occured in adminLogin () method " + e);
		}
		return false;
	   
	}
	
	public boolean addUserDetails() {
		String sql="insert into lms.user values(?,?,?,?,?,?,?,?)";
		try {
			    pstmt = con.prepareStatement(sql);
			    pstmt.setString(1, fullname);
				pstmt.setString(2, mobile);
				pstmt.setString(3, email);
				pstmt.setString(4, gender);
				pstmt.setString(5, dob);
				pstmt.setString(6, adhaarno);
				pstmt.setString(7, name);
				pstmt.setString(8, pwd);

			 int row = pstmt.executeUpdate();
			 if(row==1) {
				System.out.println(row +" was updated");
				 return true;
			 }
		}
		catch (Exception e) {
			System.out.println("some problem occured in addUserDetails () method " + e);
		}
		return false;
	   
	}
	
	
	public boolean addBookDetails() {
		String sql="insert into lms.bookdetail values(?,?,?,?,?)";
		try {
			 pstmt = con.prepareStatement(sql);
			    pstmt.setString(1, bookid);
				pstmt.setString(2, category);
				pstmt.setString(3, bookname);
				pstmt.setString(4, authorname);
				pstmt.setString(5, edition);
				
				int row = pstmt.executeUpdate();
				
				if(row==1) {
					System.out.println(row + "was updated");
					return true;
				}
		 }
		catch (Exception e) {
			System.out.println("some problem occured in addBookDetails () method " + e);
		}
		return false;
	}
	
	

}
