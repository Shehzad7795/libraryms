package com.abc.lms;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AddUserDetails")
public class AddUserDetails extends HttpServlet {
	public void service(HttpServletRequest request,HttpServletResponse response) throws IOException {
		String fullname = request.getParameter("fname");
		String mobile = request.getParameter("mobile");
		String email = request.getParameter("email");
		String gender = request.getParameter("gender");
		String dob = request.getParameter("dob");
		String adhaar = request.getParameter("adhaar");
		String name = request.getParameter("username");
		String pwd = request.getParameter("pass");
		Model m = new Model();
		/*
		 * if(fullname.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,10}&")){
		 * m.setFullname(fullname); } else {
		 * System.out.println("Name cannot have numbers in it"); }
		 */
		m.setFullname(fullname);
		m.setMobile(mobile);
		m.setEmail(email);
		m.setGender(gender);
		m.setDob(dob);
		m.setAdhaarno(adhaar);
		m.setName(name);
		m.setPwd(pwd);
		
		
		boolean addUserDetails = m.addUserDetails();
		if(addUserDetails==true) {
		
			response.sendRedirect("AddUserDetailsSuccess.jsp");
			
		}
		else {
			response.sendRedirect("loginFail.jsp");
		}
	}

}
