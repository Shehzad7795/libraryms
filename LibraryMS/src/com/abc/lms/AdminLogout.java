package com.abc.lms;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AdminLogout")
public class AdminLogout extends HttpServlet{

	protected void service(HttpServletRequest request, HttpServletResponse resposne)  {
		try 
		
		{
		
			resposne.sendRedirect("dashboard.html");  
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
	}
 }
