package com.abc.lms;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/AdminLogin")
public class AdminLogin extends HttpServlet{
	public void service(HttpServletRequest request,HttpServletResponse response) throws IOException {
		String adminname = request.getParameter("un");
		String adminpwd = request.getParameter("pw");
		Model m = new Model();
		m.setAdminname(adminname);
		m.setAdminpwd(adminpwd);
		boolean adminLogin = m.adminLogin();
		if(adminLogin==true) {
			String username = m.getUsername();
			HttpSession session = request.getSession();
			session.setAttribute("adminname", adminname);
			response.sendRedirect("adminDashboard.jsp");
			
		}
		else {
			response.sendRedirect("loginFail.jsp");
		}
	}
}
