package com.abc.lms;

import java.io.IOException;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserLogin extends HttpServlet{

	public void service(HttpServletRequest request,HttpServletResponse response) throws IOException {
		String name = request.getParameter("username");
		String password = request.getParameter("password");
		Model m = new Model();
		m.setUsername(name);
		m.setPassword(password);
		boolean login = m.login();
		if(login==true) {
			String username = m.getUsername();
			HttpSession session = request.getSession(true);
			session.setAttribute("username", username);
			response.sendRedirect("userDashboard.jsp");
			
		}
		else {
			response.sendRedirect("loginFail.jsp");
		}
	}
}
