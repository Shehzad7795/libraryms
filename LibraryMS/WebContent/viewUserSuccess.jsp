<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%

/*String id = request.getParameter("userid");*/
String driver = "com.mysql.jdbc.Driver"; 

/* String database = "leavemanagement"; */
    String url="jdbc:mysql://localhost:3306/lms";
    String un="root";
    String pw="root123";
    
try {
Class.forName(driver);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}
Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>
</head>
<body>

<h1>Retrieve data from database for user view</h1>
<table border="1">
<tr>

               <th>FullName</th>
               <th>Mobile</th>
               <th>Email</th>
               <th>Gender</th>
               <th>DOB</th>
               <th>AdhaarNo</th>
               <th>UserName</th>
               <th>Password</th>
             

</tr>
<%
try{
connection = DriverManager.getConnection(url, un, pw);
statement=connection.createStatement();
String sql ="select * from lms.user";
resultSet = statement.executeQuery(sql);
while(resultSet.next()){
%>
<tr>
<td><%=resultSet.getString("fullname") %></td>
<td><%=resultSet.getString("mobile") %></td>
<td><%=resultSet.getString("email_id") %></td>
<td><%=resultSet.getString("gender") %></td>
<td><%=resultSet.getString("dob") %></td>
<td><%=resultSet.getString("adhaar_number") %></td>
<td><%=resultSet.getString("username") %></td>
<td><%=resultSet.getString("password") %></td>
<td><button id="update">Update</button></td>
<td><button id="delete">Delete</button></td>
</tr>

<%
}
connection.close();
} 
catch (Exception e) {
e.printStackTrace();
}
%>

</table>
</body>
</html>
	