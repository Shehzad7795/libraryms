<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%

/*String id = request.getParameter("userid");*/
String driver = "com.mysql.jdbc.Driver"; 

/* String database = "leavemanagement"; */
    String url="jdbc:mysql://localhost:3306/lms";
    String un="root";
    String pw="root123";
    
try {
Class.forName(driver);
} catch (ClassNotFoundException e) {
e.printStackTrace();
}
Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<!DOCTYPE html>
<html>
<head>
<style>
table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {background-color: #f2f2f2;}
</style>
</head>
<body>

<h1>Retrieve data from database for view book</h1>
<table border="1">
<tr>

               <th>BookId</th>
               <th>Category</th>
               <th>Book Name</th>
               <th>Author Name</th>
               <th>Edition</th>
               
              

</tr>
<%
try{
connection = DriverManager.getConnection(url, un, pw);
statement=connection.createStatement();
String sql ="select * from lms.bookdetail";
resultSet = statement.executeQuery(sql);
while(resultSet.next()){
%>
<tr>
<td><%=resultSet.getString("bookid") %></td>
<td><%=resultSet.getString("category") %></td>
<td><%=resultSet.getString("bookname") %></td>
<td><%=resultSet.getString("authorname") %></td>
<td><%=resultSet.getString("edition") %></td>

</tr>
<%
}
connection.close();
} 
catch (Exception e) {
e.printStackTrace();
}
%>

</table>
</body>
</html>
	