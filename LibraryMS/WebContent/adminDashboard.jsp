<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Online Library Management System | User Dash Board</title>
    <!-- BOOTSTRAP CORE STYLE  -->
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <!-- FONT AWESOME STYLE  -->
    <link href="assets/css/font-awesome.css" rel="stylesheet" />
    <!-- CUSTOM STYLE  -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />

</head>
<body background="">
      <!------MENU SECTION START-->
   Welcome 
  <% 
 
  session=request.getSession();
  out.println(session.getAttribute("adminname"));
  %>
  
 
<!-- MENU SECTION END-->
    <div class="content-wrapper">
         <div class="container">
        <div class="row pad-botm">
            <div class="col-md-12">
                <h4 class="header-line">ADMIN DASHBOARD</h4>
                
                            </div>

        </div>
             
             <div class="row">



            
                 <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-error back-widget-set text-center">
                            <i class="fa fa-book fa-5x"></i><br>

                            <a href="issuedBooks.jsp" >Book Issued</a>
                           
                        </div>
                    </div>


                    

                       <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-error back-widget-set text-center">
                            <i class="fa fa-book fa-5x"></i><br>
                            
                       <a href="addBook.html" >Add Book</a>
                            
                             
                        </div>
                    </div>
                    
                     <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-error back-widget-set text-center">
                            <i class="fa fa-book fa-5x"></i><br>
                            
                       <a href="viewBookSuccess.jsp"> View Book</a>
                            
                             
                        </div>
                    </div>
                    
                    

                      <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-error back-widget-set text-center">
                            <i class="fa fa-user fa-5x"></i><br>

                       <a href="AdminLogout"> Admin Logout</a>
                          
                        
                        </div>
                    </div>
             
             
                     <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-error back-widget-set text-center">
                            <i class="fa fa-user fa-5x"></i><br>

                            
                            <a href="addUser.html" >Add User</a>
                        </div>
                    </div>


                      <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-error back-widget-set text-center">
                            <i class="fa fa-user fa-5x"></i><br>

                       <a href="changeAdminPassword.jsp"> ChangePassword</a>
                          
                        
                        </div>
                    </div>
                    
                    
                    <div class="col-md-3 col-sm-3 col-xs-6">
                      <div class="alert alert-error back-widget-set text-center">
                            <i class="fa fa-user fa-5x"></i><br>

                       <a href=" viewUserSuccess.jsp"> View User</a>
                          
                        
                        </div>
                    </div>


                    




        </div>


            
    </div>
    </div>
     <!-- CONTENT-WRAPPER SECTION END-->

      <!-- FOOTER SECTION END-->
    <!-- JAVASCRIPT FILES PLACED AT THE BOTTOM TO REDUCE THE LOADING TIME  -->
    <!-- CORE JQUERY  -->
    <script src="assets/js/jquery-1.10.2.js"></script>
    <!-- BOOTSTRAP SCRIPTS  -->
    <script src="assets/js/bootstrap.js"></script>
      <!-- CUSTOM SCRIPTS  -->
    <script src="assets/js/custom.js"></script>
</body>
</html>